package com.example.nandamochammad.tabmbakeni;

import android.graphics.drawable.Drawable;

/**
 * Created by Nanda Mochammad on 10/26/2016.
 */

public class ObjectListView {

    private String namaPraktikan;
    private String namaPraktikum;
    private String Deskripsi;
    private Drawable gambarPrak;

    public ObjectListView(String namaPraktikan, String namaPraktikum, String Deskripsi, Drawable gambarPrak){
        this.namaPraktikan = namaPraktikan;
        this.namaPraktikum = namaPraktikum;
        this.Deskripsi = Deskripsi;
        this.gambarPrak = gambarPrak;
    }

    public String getNamaPraktikan() {
        return namaPraktikan;
    }

    public void setNamaPraktikan(String namaPraktikan) {
        this.namaPraktikan = namaPraktikan;
    }

    public String getNamaPraktikum() {
        return namaPraktikum;
    }

    public void setNamaPraktikum(String namaPraktikum) {
        this.namaPraktikum = namaPraktikum;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public Drawable getGambarPrak() {
        return gambarPrak;
    }

    public void setGambarPrak(Drawable gambarPrak) {
        this.gambarPrak = gambarPrak;
    }
}
