package com.example.nandamochammad.tabmbakeni;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.nandamochammad.tabmbakeni.R.id.container;

public class MainActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private int[] tabIcons = {R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher};

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    //*****************************************INI CODE UNTUK LIST VIEW*********************************************

    ArrayList<ObjectListView> arrayListData = new ArrayList<>();

    void addData(String namaPraktikan, String namaPraktikum, String Deskripsi, Drawable gambarPrak) {
        ObjectListView addData = new ObjectListView(namaPraktikan, namaPraktikum, Deskripsi, gambarPrak);

        arrayListData.add(addData);
    }
    Context context;

    //*************************************************************************************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.addFragment(new Fragment1());
        mSectionsPagerAdapter.addFragment(new Fragment2());
        mSectionsPagerAdapter.addFragment(new Fragment3());


        mViewPager = (ViewPager) findViewById(container);
        mViewPager.setAdapter(mSectionsPagerAdapter);



        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

        //*****************************************INI CODE UNTUK LIST VIEW*********************************************

        context = MainActivity.this;

        ListView listView = (ListView)findViewById(R.id.ListView);

        BaseAdapter baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return arrayListData.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View itemCustom = getLayoutInflater().inflate(R.layout.activity_object_list, null);

                TextView namaPraktikan = (TextView)findViewById(R.id.nama);
                namaPraktikan.setText(arrayListData.get(position).getNamaPraktikan());

                TextView namaPraktikum = (TextView)findViewById(R.id.namaPrak);
                namaPraktikum.setText(arrayListData.get(position).getNamaPraktikum());

                ImageView gambarPrak = (ImageView) findViewById(R.id.objekGambar1);
                gambarPrak.setImageDrawable(arrayListData.get(position).getGambarPrak());

                return itemCustom;
            }
        };
        listView.setAdapter(baseAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, DetailListView.class);
                intent.putExtra("namaPraktikan", arrayListData.get(position).getNamaPraktikan());
                intent.putExtra("namaPraktikum", arrayListData.get(position).getNamaPraktikum());
                intent.putExtra("Deskripsi", arrayListData.get(position).getDeskripsi());
                intent.putExtra("gambarPrak", String.valueOf(arrayListData.get(position).getGambarPrak()));
               startActivity(intent);
            }
        });

    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> mfragment = new ArrayList<>();

        public void addFragment(Fragment fragment) {
            mfragment.add(fragment);
        }

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        }
    }

